
# Create a VPC
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "main"
  }
}


##SUBNET
resource "aws_subnet" "public_subnet" {
  count = 3
  vpc_id            = local.vpc_id
  cidr_block        = var.public_subnet[count.index]
  availability_zone = [local.azs[0],local.azs[1], local.azs[2]] [count.index] #data.aws_availability_zones.available.names[0]

  tags = {
    "Name" = "public_subnet"
  }

}


resource "aws_subnet" "private_subnet" {
  count = 3
  vpc_id            = local.vpc_id
  cidr_block        = var.private_subnet[count.index]
  availability_zone = [local.azs[0],local.azs[1], local.azs[2]] [count.index]      #data.aws_availability_zones.available.names[0]

  tags = {
    "Name" = "private_subnet"
  }

}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ami.id
  instance_type = "t3.micro"

  tags = {
    Name = "Web"
  }
}