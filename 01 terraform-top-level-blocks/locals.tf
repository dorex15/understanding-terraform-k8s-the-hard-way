
locals {
   vpc_id = aws_vpc.main.id
   azs = data.aws_availability_zones.available.names
}
