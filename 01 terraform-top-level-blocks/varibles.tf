variable "vpc_cidr_block" {
    description = "vpc cidr block."
    default =  "10.0.0.0/16"
}


variable "private_subnet" {
  type = list
  default = ["10.0.1.0/24", "10.0.3.0/24", "10.0.5.0/24"]
  description = "Value of subnet 1 cidr"
}

variable "public_subnet" {
  type = list
  default = ["10.0.0.0/24", "10.0.2.0/24", "10.0.4.0/24"]
  description = "Value of subnet 2 cidr"
}



# variable "subnet_1_az" {
#   type = string
#   default = "us_east_1a"
#   description = "value"
# }

# variable "subnet_2_az" {
#   type = string
#   default = "us_east_1b"
#   description = "value"

# }