
variable "vpc_cidr_block" {
    description = "vpc cidr block."
    default =  "10.0.0.0/16"
}


variable "subnet_1_cidr" {
  type = string
  default = "10.0.1.0/24"
  description = "Value of subnet 1 cidr"
}

variable "subnet_2_cidr" {
  type = string
  default = "10.0.2.0/24"
  description = "Value of subnet 2 cidr"
}



# variable "subnet_1_az" {
#   type = string
#   default = "us_east_1a"
#   description = "value"
# }

# variable "subnet_2_az" {
#   type = string
#   default = "us_east_1b"
#   description = "value"

# }